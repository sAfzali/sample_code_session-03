package com.sample.afzali.samplecode.database;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sample.afzali.samplecode.R;
import com.sample.afzali.samplecode.utils.BaseActivity;
import com.sample.afzali.samplecode.utils.Cons;
import com.sample.afzali.samplecode.utils.PublicMethods;

import java.util.List;

public class DatabaseActivity extends BaseActivity implements View.OnClickListener {
    EditText userName, email, address;
    TextView result;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        dbNewHandler();
        bind();

    }

    void dbNewHandler() {
        db = new DatabaseHandler(mContext, Cons.dbName, null, 1);
    }

    void bind() {
        userName = findViewById(R.id.user_name);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);
        result = findViewById(R.id.result);
        findViewById(R.id.save).setOnClickListener(this);
//        load();
        loadBySugar();
    }

    @Override
    public void onClick(View v) {
/*
save(userName.getText().toString(),
email.getText().toString(),
address.getText().toString());
*/
        saveBySugar(userName.getText().toString(),
                email.getText().toString(),
                address.getText().toString());
    }

    public void load() {
        result.setText(db.getUsers());
    }

    private void save(String userName, String email, String address) {
        db.addUser(userName, email, address);
        clean();
    }

    public void saveBySugar(String userName, String email, String address) {
        UserEntity user = new UserEntity(userName, email, address);
        user.save();
        clean();
        loadBySugar();
    }

    public void loadBySugar() {
        result.setText("");
        List<UserEntity> users = UserEntity.listAll(UserEntity.class);
        for (UserEntity user : users) {
            result.append(
                    user.getUserName()
                            + " " + user.getEmail()
                            + " " + user.getAddress()
                            + "/n");
        }
    }

    void clean() {
        PublicMethods.toast(mContext, "New user has been added");
        this.userName.setText("");
        this.address.setText("");
        this.email.setText("");
    }


}
