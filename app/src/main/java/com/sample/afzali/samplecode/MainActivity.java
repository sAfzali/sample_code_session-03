package com.sample.afzali.samplecode;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    protected EditText userName;
    protected EditText family;
    protected TextView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
    }

    private void bind() {
        userName = findViewById(R.id.user_name);
        userName.setText(getString(R.string.soheil_afzali));
        family = findViewById(R.id.family);
        details = findViewById(R.id.details);
        details.setText(getString(R.string.set_text_for_text_view));
        details.setOnClickListener(this);
        findViewById(R.id.show_details).setOnClickListener(this);
/*
((TextView)findViewById(R.id.details)).setOnClickListener(this);
findViewById(R.id.details).setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

}
});
findViewById(R.id.details).setOnClickListener(V->{
family.setText(getString(R.string.my_family));
});
*/
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.show_details) {
            details.setText(getString(R.string.after_click));
        } else if (v.getId() == R.id.details) {
            Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();
/*
String titleValue = family.getText().toString();
tost(this, titleValue);
*/
        }
    }


    private void tost(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

}
