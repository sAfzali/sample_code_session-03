
package com.sample.afzali.samplecode.intentsample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sample.afzali.samplecode.R;

public class OriginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText name, family;
    Button showDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origin);
        bind();
    }

    void bind() {
        name = findViewById(R.id.name);
        family = findViewById(R.id.family);
        showDetails = findViewById(R.id.show_details);
        showDetails.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String userNameValue = name.getText().toString();
        String familyValue = family.getText().toString();
        Intent intent = new Intent(this, DestinationActivity.class);
        intent.putExtra("userName", userNameValue);
        intent.putExtra("family", familyValue);
        startActivity(intent);
    }
}
