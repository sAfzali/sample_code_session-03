package com.sample.afzali.samplecode.foodslist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sample.afzali.samplecode.R;

import java.util.List;

public class FoodsAdapter extends BaseAdapter {
    public Context mContext;
    List<FoodModel> foods;

    public FoodsAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View v = LayoutInflater.from(mContext).inflate(R.layout.foods_list_item, parent, false);
        TextView name = v.findViewById(R.id.name);
        TextView price = v.findViewById(R.id.price);
        ImageView image = v.findViewById(R.id.image);
        name.setText(foods.get(position).getName());
        price.setText(foods.get(position).getPrice()+" ");
        Glide.with(mContext).load(foods.get(position).getImage()).into(image);
        return v;
    }
}
