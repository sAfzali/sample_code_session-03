package com.sample.afzali.samplecode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sample.afzali.samplecode.utils.BaseActivity;
import com.sample.afzali.samplecode.utils.PublicMethods;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class IMDBActivity extends BaseActivity {
    TextView title, year, director, movie;
    ImageView posetr;
    String result = "faild";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imdb);
        bind();
    }

    void bind() {
        movie = findViewById(R.id.movie);
        title = findViewById(R.id.title);
        year = findViewById(R.id.year);
        director = findViewById(R.id.director);
        posetr = findViewById(R.id.poster);
        findViewById(R.id.show_details).setOnClickListener(V -> {
            load(movie.getText().toString());
        });
    }

    void load(String movie) {
        String url = "http://www.omdbapi.com/?i=" + movie + "&apikey=2cc69df0";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(mContext, result);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                show(responseString);
            }
        });

    }

    void show(String respone) {
        try {
            JSONObject json = new JSONObject(respone);
        } catch (Exception e) {

        }

    }


}
