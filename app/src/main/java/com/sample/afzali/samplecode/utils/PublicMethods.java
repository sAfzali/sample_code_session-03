package com.sample.afzali.samplecode.utils;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class PublicMethods {


    public static void toast(Context mcontext, String msg) {
        Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();
    }

    public static void saveData(String key, String value) {
        Hawk.put(key, value);
/*
PreferenceManager.getDefaultSharedPreferences(mContext)
.edit().putString(key, value).apply();
*/
    }

    public static String getData(String key, String value) {
        return Hawk.get(key, value);
/*
return PreferenceManager.getDefaultSharedPreferences(mContext)
.getString(key, value);
*/
    }

    public static String url(double lat, double lng) {
        return "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather." +
                "forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places%20where" +
                "%20text%3D%22(" + lat + "%2C%20" + lng + ")%22)&format=json&env=store%3A%2F%2Fdatatables." +
                "org%2Falltableswithkeys";
    }
}
