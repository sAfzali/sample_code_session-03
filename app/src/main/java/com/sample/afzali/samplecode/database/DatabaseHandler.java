package com.sample.afzali.samplecode.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
    String tableQuery = " " +
            " CREATE TABLE users (" +
            " id INTEGER PRIMARY KEY AUTOINCREMENT , " +
            " username TEXT , " +
            " email TEXT , " +
            " address TEXT " +
            " )";

    DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(tableQuery);
    }

    public void addUser(String userName, String email, String address) {
        SQLiteDatabase db = this.getWritableDatabase();
        String addUserQuery = "" +
                "INSERT INTO users(username,address,email) " +
                "VALUES( '" + userName + "' , '" + address + "' , '" + email + "' ) ";
        db.execSQL(addUserQuery);
        db.close();
    }

    public String getUsers() {
        StringBuilder result = new StringBuilder();
        SQLiteDatabase db = this.getReadableDatabase();
        String usersQuery = "SELECT username,email,address FROM users";
        Cursor cursor = db.rawQuery(usersQuery, null);
        while (cursor.moveToNext()) {
            result.append(cursor.getString(0)).append(" ").
                    append(cursor.getString(1)).append(" ").
                    append(cursor.getString(2)).append("\n");
        }
        return result.toString();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
