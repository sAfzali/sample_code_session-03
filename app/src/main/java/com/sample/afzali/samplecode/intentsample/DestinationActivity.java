package com.sample.afzali.samplecode.intentsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.sample.afzali.samplecode.R;

import java.text.MessageFormat;

public class DestinationActivity extends AppCompatActivity {
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);
        bind();
    }

    void bind() {
        result = findViewById(R.id.result);
        String userName = getIntent().getStringExtra("userName");
        String family = getIntent().getStringExtra("family");
        result.setText(MessageFormat.format("{0} {1}", userName, family));
    }
}
