package com.sample.afzali.samplecode;

import android.content.Context;
import android.content.Intent;
import android.graphics.Region;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.MessageFormat;

public class PractiseCoding extends AppCompatActivity implements View.OnClickListener {
    EditText editText;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_coding);
        Log.d("monitor_tag", "onCreate: ");
        publicMethods();
    }


    protected void publicMethods() {
        TextView textView = findViewById(R.id.result);
        String titleValue = textView.getText().toString();
        textView.setText(getString(R.string.soheil_afzali));

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.result) {
                    textView.setText(getString(R.string.after_click));
                }
            }
        });
        textView.setOnClickListener(this);
        textView.setOnClickListener(v -> {
            textView.setText(getString(R.string.set_text_for_text_view));
        });


    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        String userNameValue = editText.getText().toString();
        String familyValue = editText.getText().toString();
        intent.putExtra("userName", userNameValue);
        intent.putExtra("family", familyValue);
        startActivity(intent);

        //go to destination and definition statements
        String name = getIntent().getStringExtra("userName");
        String family = getIntent().getStringExtra("family");
        textView.setText(MessageFormat.format("{0}{1}", userNameValue, familyValue));

    }


    void toast(Context mcontext, String msg) {
        Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("monitor_tag", "onStart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("monitor_tag", "onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("monitor_tag", "onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("monitor_tag", "onStop: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("monitor_tag", "onDestroy: ");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
