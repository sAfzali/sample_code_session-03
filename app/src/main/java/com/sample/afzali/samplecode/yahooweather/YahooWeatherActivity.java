package com.sample.afzali.samplecode.yahooweather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sample.afzali.samplecode.R;
import com.sample.afzali.samplecode.utils.BaseActivity;
import com.sample.afzali.samplecode.utils.Cons;
import com.sample.afzali.samplecode.utils.PublicMethods;
import com.sample.afzali.samplecode.yahooweather.models.Forecast;
import com.sample.afzali.samplecode.yahooweather.models.YahooModel;

import cz.msebera.android.httpclient.Header;

public class YahooWeatherActivity extends BaseActivity {
    EditText city;
    TextView temp;
    ListView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yahoo_weather);
        bind();
    }


    void bind() {
        city = findViewById(R.id.city);
        temp = findViewById(R.id.temp);
        result = findViewById(R.id.result);
        findViewById(R.id.show_weather).setOnClickListener(V -> {
            showWeatherByYahoo(city.getText().toString());
        });
    }

    private void showWeatherByYahoo(String city) {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = Cons.URLPIECEONE + city + Cons.URLPIECETWO;
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(mContext, Cons.FAILURE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndLoadData(responseString);
            }
        });
    }

    private void parseAndLoadData(String response) {
        try {
            Gson gson = new Gson();
            YahooModel yahoo = new YahooModel();
            gson.fromJson(response, YahooModel.class);
            if (yahoo.getQuery().getCount() == 1) {
                String temp = yahoo.getQuery().getResults().getChannel()
                        .getItem().getCondition().getTemp();
                this.temp.setText("Temp is " + temp + "F");
                YahooAdapter adapter = new YahooAdapter(mContext,
                        yahoo.getQuery().getResults().getChannel().getItem()
                                .getForecast());
                result.setAdapter(adapter);
            } else
                PublicMethods.toast(mContext, Cons.FAILED);

        } catch (Exception e) {
            PublicMethods.toast(mContext, Cons.FAILED);
        }
    }
}
