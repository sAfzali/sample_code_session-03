package com.sample.afzali.samplecode.userdetail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sample.afzali.samplecode.R;
import com.sample.afzali.samplecode.utils.BaseActivity;
import com.sample.afzali.samplecode.utils.PublicMethods;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class UserDetailRestActivity extends BaseActivity {
    TextView ip, isp, country, city;
    String result = "Erroring in receiving data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail_rest);
        bind();
        load();
    }

    public void load() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://ip-api.com/json", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                PublicMethods.toast(mContext, result);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                PublicMethods.toast(mContext, responseString);
                parseAndShowResults(responseString);

            }
        });
    }

    void parseAndShowResults(String response) {
        try {
            JSONObject object = new JSONObject(response);
            String ipValue = object.getString("query");
            String ispValue = object.getString("isp");
            String countryValue = object.getString("country");
            String cityValue = object.getString("city");
            ip.setText(ipValue);
            isp.setText(ispValue);
            country.setText(countryValue);
            city.setText(cityValue);
        } catch (Exception e) {
            PublicMethods.toast(mContext, result);
        }

    }

    void bind() {
        ip = findViewById(R.id.ip);
        isp = findViewById(R.id.isp);
        country = findViewById(R.id.country);
        city = findViewById(R.id.city);
        findViewById(R.id.reload).setOnClickListener(V -> {
            load();
        });
    }
}
