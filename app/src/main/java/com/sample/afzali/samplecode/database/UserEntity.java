package com.sample.afzali.samplecode.database;

import com.orm.SugarRecord;

public class UserEntity extends SugarRecord<UserEntity> {
    String userName, email, address;

    public UserEntity(String userName, String email, String address) {
        this.userName = userName;
        this.email = email;
        this.address = address;
    }

    public UserEntity() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
