package com.sample.afzali.samplecode.yahooweather;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sample.afzali.samplecode.R;
import com.sample.afzali.samplecode.utils.Cons;
import com.sample.afzali.samplecode.yahooweather.models.Forecast;
import com.sample.afzali.samplecode.yahooweather.models.YahooModel;

import java.util.List;

public class YahooAdapter extends BaseAdapter {
    private Context mContext;
    private List<Forecast> forcasts;


    YahooAdapter(Context mContext, List<Forecast> forcasts) {
        this.mContext = mContext;
        this.forcasts = forcasts;
    }

    @Override
    public int getCount() {
        return forcasts.size();
    }

    @Override
    public Object getItem(int position) {
        return forcasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View v = LayoutInflater.from(mContext).inflate(R.layout.item_yahoomodels_list, parent, false);
        TextView date = v.findViewById(R.id.date);
        TextView status = v.findViewById(R.id.status);
        TextView min = v.findViewById(R.id.min);
        TextView max = v.findViewById(R.id.max);
        ImageView icon = v.findViewById(R.id.icon);

        Forecast data = forcasts.get(position);
        date.setText(data.getDate());
        status.setText(data.getDate());
        min.setText(data.getDate());
        max.setText(data.getDate());
        String img = Cons.IMGURLCLOUDY;
        if (data.getText().toLowerCase().contains("cloudy"))
            img = Cons.IMGURLSHOWERS;
        Glide.with(mContext).load(img).into(icon);
        return v;
    }
}
